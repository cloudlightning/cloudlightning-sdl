tosca_definitions_version: alien_dsl_1_3_0
description: TOSCA Demo 2016
template_name: cloudlightning-intel-types
template_version: 1.0.0-SNAPSHOT
template_author: AdrianSpataru

imports:
  - "tosca-normative-types:1.0.0-SNAPSHOT"
  - "cl-normative-types:1.0.0-SNAPSHOT"
  - "docker-types:1.2.0-SNAPSHOT"

repositories:
  docker:
    url: https://hub.docker.com/
    type: http

node_types:    
  cloudlightning.nodes.meta.RayTracingEngine:
    abstract: true
    derived_from: cloudlightning.nodes.CLService
    tags:
      icon: images/raytrace.png
    description: Abstract RayTracing Node
    capabilities:
      raytrace_engine: tosca.capabilities.Endpoint
      service: cloudlightning.capabilities.CLService.RayTracingEngine

  cloudlightning.nodes.meta.RayTracingUI:
    abstract: true
    derived_from: cloudlightning.nodes.CLService
    tags:
      icon: images/raytrace_ui.png
    description: Abstract RayTracing UI Node
    capabilities:
      service: cloudlightning.capabilities.CLService.RayTracingUI
    requirements:
      raytrace_engine: 
        capability: tosca.capabilities.Endpoint
        relationship: tosca.relationships.ConnectsTo
  
  cloudlightning.nodes.SimpleRayTracingEngine:
    derived_from: cloudlightning.nodes.CLContainer
    tags:
      icon: images/raytrace_intel.png
      performance: 1.0
    properties:
      cpu_share:
        type: float
        required: true
        default: 0.1
      mem_share:
        type: scalar-unit.size
        required: true
        default: 512 MB
    capabilities:
      raytrace_engine: cloudlightning.capabilities.endpoint.docker.RayTracing
      service: cloudlightning.capabilities.CLService.RayTracingEngine
    interfaces:
      Standard:
        create:
          inputs:
            OPT_hostname : "embree_renderer.weave.local"
          implementation:
            file: "pkudaiyar/ray_tracing_app_embree:1"
            repository: docker
            type: tosca.artifacts.Deployment.Image.Container.Docker

  cloudlightning.nodes.PhiRayTracingEngine:
    derived_from: cloudlightning.nodes.MICContainer
    tags:
      icon: images/raytrace_intel_phi.png
      performance: 4.0
    description: Embree based RayTracing Node With Intel Phi Acceleration
    properties:
      cpu_share:
        type: float
        required: true
        default: 0.1
      mem_share:
        type: scalar-unit.size
        required: true
        default: 512 MB
    # this can be ommited as is inherrited from SimpleRayTracingEngine
    capabilities:
      raytrace_engine: cloudlightning.capabilities.endpoint.docker.RayTracing
      service: cloudlightning.capabilities.CLService.RayTracingEngine
    interfaces:
      Standard:
        create:
          inputs:
            OPT_hostname : "embree_renderer.weave.local" # not inherited 
            # CONSTRAINT_mic_CLUSTER: "available"
          implementation:
            file: "pkudaiyar/ray_tracing_app_embree:1"
            repository: docker
            type: tosca.artifacts.Deployment.Image.Container.Docker


  cloudlightning.nodes.RayTracingWebservice:
    derived_from: cloudlightning.nodes.CLContainer
    tags:
      icon: images/raytrace_ui_docker.png
      performance: 1.0
    properties:
      cpu_share:
        type: float
        required: true
        default: 0.1
      mem_share:
        type: scalar-unit.size
        required: true
        default: 512 MB
    capabilities:
      service: cloudlightning.capabilities.CLService.RayTracingUI
      WebUI_endpoint: cloudlightning.capabilities.endpoint.docker.WebUI
    attributes:
      WebUi: {concat : ["http://", get_property : [SELF, WebUI_endpoint, ip_address], ":", get_property : [SELF, WebUI_endpoint, port]]}
    requirements:
      raytrace_engine: 
        capability: cloudlightning.capabilities.endpoint.docker.RayTracing
        relationship: tosca.relationships.ConnectsTo
    interfaces:
      Standard:
        create:
          inputs:
            OPT_hostname : "raytracing_webservice.weave.local"
            ENV_RT_ENGINE: { get_property: [REQ_TARGET, raytrace_engine, ip_address]}
            ENV_RT_ENGINE_PORT: { get_property: [REQ_TARGET, raytrace_engine, port]}
          implementation:
            file: "adispataru/ray_tracing_app:2.2"
            repository: docker
            type: tosca.artifacts.Deployment.Image.Container.Docker

  cloudlightning.nodes.RayTracingWebserviceSoftware:
    derived_from: cloudlightning.nodes.CLService.CLSoftware
    tags:
      icon: images/raytrace_ui.png
      performance: 1.0
    capabilities:
      service: cloudlightning.capabilities.CLService.RayTracingUI
      WebUI_endpoint: cloudlightning.capabilities.endpoint.WebUI
    attributes:
      WebUi: {concat : ["http://", get_attribute : [SELF, host.address], ":", get_property : [SELF, WebUI_endpoint, port]]}
    requirements:
      raytrace_engine: 
        capability: cloudlightning.capabilities.endpoint.docker.RayTracing
        relationship: tosca.relationships.ConnectsTo
    interfaces:
      Standard:
        create:
          inputs:
            RT_ENGINE: { get_property: [REQ_TARGET, raytrace_engine, ip_address]}
            RT_ENGINE_PORT: { get_property: [REQ_TARGET, raytrace_engine, port]}
          implementation: scripts/create_vm_raytracing.sh
        stop:
          implementation: scripts/stop_vm_raytracing.sh


capability_types:
  cloudlightning.capabilities.CLService.RayTracingEngine:
    derived_from: cloudlightning.capabilities.CLService
  cloudlightning.capabilities.CLService.RayTracingUI:
    derived_from: cloudlightning.capabilities.CLService
  cloudlightning.capabilities.endpoint.docker.RayTracing:
    derived_from: cloudlightning.capabilities.endpoint.docker.SSH
    properties:
      docker_bridge_port_mapping:
        type: integer
        description: Port used to bridge to the container's endpoint.
        default: 31009
  cloudlightning.capabilities.endpoint.docker.RTRegisterEndpoint:
    derived_from: alien.capabilities.endpoint.Docker
    description: >
      Capability to register a Docker-based Ray Tracing engine.
    properties:
      docker_bridge_port_mapping:
        type: integer
        description: Port used to bridge to the container's endpoint.
        default: 0
      port:
        type: integer
        default: 9393
  cloudlightning.capabilities.endpoint.docker.WebUI:
    derived_from: alien.capabilities.endpoint.Docker
    description: >
      Capability to connect to a Docker-supported Mongo database through bridge networking.
    properties:
      docker_bridge_port_mapping:
        type: integer
        description: Port used to bridge to the container's endpoint.
        default: 0
      port:
        type: integer
        default: 3005
  cloudlightning.capabilities.endpoint.WebUI:
    derived_from: tosca.capabilities.Endpoint
    description: >
      Capability to connect to a Docker-supported Mongo database through bridge networking.
    properties:
      port:
        type: integer
        default: 3005


  